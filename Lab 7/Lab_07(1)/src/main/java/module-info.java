module com.example.lab_08 {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.lab_08 to javafx.fxml;
    exports com.example.lab_08;
    exports com.example.lab_08.utils;
    exports com.example.lab_08.models;
    opens com.example.lab_08.models to javafx.fxml;
    exports com.example.lab_08.controllers;
    opens com.example.lab_08.controllers to javafx.fxml;
}