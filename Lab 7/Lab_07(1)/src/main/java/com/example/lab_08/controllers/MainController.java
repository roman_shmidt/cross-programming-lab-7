package com.example.lab_08.controllers;

import com.example.lab_08.models.AlgorithmModel;
import com.example.lab_08.models.ThreadMonitorModel;
import javafx.beans.binding.Binding;
import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableListBase;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class MainController {
    @FXML
    private final ObjectProperty<AlgorithmModel> algorithm = new SimpleObjectProperty<>();
    @FXML
    private final StringProperty threadCount = new SimpleStringProperty("");

    public MainController() {
    }

    @FXML
    private void apply() {
        if (Integer.valueOf(getThreadCount()) > 0 && Integer.valueOf(getThreadCount()) < 10)
            setAlgorithm(new AlgorithmModel(Integer.valueOf(getThreadCount())));
    }

    @FXML
    private void initialize() {
    }

    // Getters and setters region
    public AlgorithmModel getAlgorithm() {
        return algorithm.get();
    }

    public ObjectProperty<AlgorithmModel> algorithmProperty() {
        return algorithm;
    }

    public void setAlgorithm(AlgorithmModel algorithm) {
        this.algorithm.set(algorithm);
    }

    public String getThreadCount() {
        return threadCount.get();
    }

    public StringProperty threadCountProperty() {
        return threadCount;
    }

    public void setThreadCount(String threadCount) {
        this.threadCount.set(threadCount);
    }
}