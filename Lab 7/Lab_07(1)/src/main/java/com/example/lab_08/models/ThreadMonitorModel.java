package com.example.lab_08.models;

import javafx.beans.property.*;

import java.util.ArrayList;

public class ThreadMonitorModel implements Runnable {
    private final StringProperty threadName = new SimpleStringProperty("");
    private final StringProperty threadState = new SimpleStringProperty("");
    private final IntegerProperty threadPriority = new SimpleIntegerProperty(-1);
    private final BooleanProperty isThreadAlive = new SimpleBooleanProperty(false);
    ArrayList<Thread> _threadsToMonitor;
    private boolean _isActive;
    private int _threadNum;
    private final long _sleepTime = 500; //0,5 sec

    public ThreadMonitorModel(ArrayList<Thread> threadsToMonitor, int threadNum) {
        _threadsToMonitor = threadsToMonitor;
        _threadNum = threadNum;
        _isActive = false;
    }

    @Override
    public void run() {
        start();
    }

    // Starts monitoring over the thread
    private void start() {
        _isActive = true;

        while (_isActive) {
            setThreadName(_threadsToMonitor.get(_threadNum).getName());
            setThreadState(String.valueOf(_threadsToMonitor.get(_threadNum).getState()));
            setThreadPriority(_threadsToMonitor.get(_threadNum).getPriority());
            setIsThreadAlive(_threadsToMonitor.get(_threadNum).isAlive());

            try {
                Thread.sleep(_sleepTime);
            } catch (InterruptedException e) {
                //System.out.println("Exception in monitor thread");
            }
        }
    }

    // Stops monitoring
    public void stop() {
        _isActive = false;
    }

    // Getters and setters region
    public StringProperty threadNameProperty() {
        return threadName;
    }

    public StringProperty threadStateProperty() {
        return threadState;
    }

    public IntegerProperty threadPriorityProperty() {
        return threadPriority;
    }

    public BooleanProperty isThreadAliveProperty() {
        return isThreadAlive;
    }

    public String getThreadName() {
        return threadName.get();
    }

    public void setThreadName(String threadName) {
        this.threadName.set(threadName);
    }

    public String getThreadState() {
        return threadState.get();
    }

    public void setThreadState(String threadState) {
        this.threadState.set(threadState);
    }

    public int getThreadPriority() {
        return threadPriority.get();
    }

    public void setThreadPriority(int threadPriority) {
        this.threadPriority.set(threadPriority);
    }

    public boolean isIsThreadAlive() {
        return isThreadAlive.get();
    }

    public void setIsThreadAlive(boolean isThreadAlive) {
        this.isThreadAlive.set(isThreadAlive);
    }
}
