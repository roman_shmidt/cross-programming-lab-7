package com.example.lab_08.algorithms;

import java.util.ArrayList;
import static java.lang.Character.toLowerCase;

// Class to store solution for DeijkstraAlgorithm
public class DijkstraSolution {
    private char _startNode;
    public ArrayList<Node> NodeList;

    public DijkstraSolution(int nodesCount, char startingNode)
    {
        _startNode = startingNode;
        this.NodeList = new ArrayList<Node>();
        char node = 'a';

        for (int i = 0; i < nodesCount; i++)
        {
            if (toLowerCase(startingNode) == node)
                NodeList.add(new Node(node, 0));
            else
                NodeList.add(new Node(node, Double.POSITIVE_INFINITY));

            node++;
        }
    }

    public String  toString()
    {
        String solution = "";

        solution += "Nodes\n";
        for (int i = 0; i < this.NodeList.size(); i++) {
            solution += i +". " + NodeList.get(i).getName() +
                    ",\t value: " + NodeList.get(i).ShortestPath +
                    ",\t prev node: " + NodeList.get(i).PrevNode + "\n";
        }

        return solution;
    }

    public char getStartNode() {
        return _startNode;
    }
}