package com.example.lab_08.algorithms;

import com.example.lab_08.utils.Pair;

import java.util.ArrayList;

public class DijkstraAlgorithm {
    private DijkstraSolution _solution;
    private ArrayList<Integer> _busyNodes;
    private ArrayList<Integer> _availableNodes;
    private ArrayList<Thread> _threads;
    private ArrayList<ArrayList<Pair<Integer, Integer>>> _adjacencyList;

    public DijkstraAlgorithm(ArrayList<ArrayList<Pair<Integer, Integer>>> adjacencyList, char startingNode, int threadCount) {
        _adjacencyList = adjacencyList;
        _solution = new DijkstraSolution(_adjacencyList.size(), startingNode);
        _busyNodes = new ArrayList<>();
        _threads = new ArrayList<>();
        _availableNodes = null;

        for (int i = 0; i < threadCount; i++)
            _threads.add(new Thread(String.valueOf(i)));
    }

    // Finds SpanningTree of the graph using threads
    public DijkstraSolution FindSolution()
    {
        _availableNodes = getAvailableNodes(_solution.NodeList);

        // iterate until there is no unvisited vertex
        while(_availableNodes.size() != 0) {
            // call ParallelTask for all available nodes on cur iteration
            for (int i = 0; i < _availableNodes.size(); ) {
                // spread tasks across all threads
                for (int j = 0; j < Math.min(_availableNodes.size(), _threads.size()) && (_availableNodes.size() - i) != 0; j++) {

                    _threads.set(j, new Thread(new ParallelTask(
                            _solution.NodeList,
                            _adjacencyList,
                            _availableNodes.get(i),
                            _busyNodes), String.valueOf(j)));

                    //System.out.println(_threads.get(j) +  "thread was set");

                    _threads.get(j).start();
                    i++;
                }

                try {
                    for (var t : _threads)
                        if (t.isAlive())
                            t.join();
                } catch (InterruptedException e) {
                    System.out.println("Interrupted");
                }
            }

            _availableNodes = getAvailableNodes(_solution.NodeList);
        }

        cleanUp();
        return _solution;
    }

    // Returns threads used by algorithm
    public ArrayList<Thread> getThreads() {
        return _threads;
    }

    // Returns available nodes to process
    private static ArrayList<Integer> getAvailableNodes(ArrayList<Node> nodeList) {
        ArrayList<Integer> availNodes = new ArrayList<>();

        for (int i = 0; i < nodeList.size(); i++) {
            if (!nodeList.get(i).isVisited && Double.isFinite(nodeList.get(i).ShortestPath)) {
                availNodes.add(i);
            }
        }

        return availNodes;
    }

    // Cleans up threads (perhaps)
    private void cleanUp() {
        for (int i = 0; i < _threads.size(); i++) {
            _threads.get(i).interrupt();
            _threads.set(i, null);
        }
    }
}

class ParallelTask implements Runnable {
    private ArrayList<Node> _nodeList;
    private ArrayList<ArrayList<Pair<Integer, Integer>>> _adjacencyList;
    private int _nodeNum;
    private ArrayList<Integer> _busyNodes;
    private final long _sleepTime = 1000;

    public ParallelTask(ArrayList<Node> nodeList,
                        ArrayList<ArrayList<Pair<Integer, Integer>>> adjacencyList,
                        int nodeNum,
                        ArrayList<Integer> busyNodes) {

        _nodeList = nodeList;
        _adjacencyList = adjacencyList;
        _nodeNum = nodeNum;
        _busyNodes = busyNodes;
    }


    // runs parallelTask() and marks node as visited
    // sleeps for _sleepTime to simulate heavy process
    @Override
    public void run() {
        parallelTask(_nodeList, _adjacencyList, _nodeNum, _busyNodes);
        _nodeList.get(_nodeNum).isVisited = true;

        try {
            Thread.sleep(_sleepTime);
        } catch (InterruptedException e) {
            System.out.println("Exception in parallel task thread");
        }
    }

    // Iterates over neighbor nodes and sets new Path if needed
    private void parallelTask(ArrayList<Node> nodeList,
                              ArrayList<ArrayList<Pair<Integer, Integer>>> adjacencyList,
                              int nodeNum,
                              ArrayList<Integer> busyNodes) {

        int curNodeNum = -1;

        for (int j = 0; j < adjacencyList.get(nodeNum).size(); j++) {
            curNodeNum = adjacencyList.get(nodeNum).get(j).getKey();
            waitIfNeeded(busyNodes, curNodeNum);

            if (!nodeList.get(curNodeNum).isVisited) {

                if((nodeList.get(nodeNum).ShortestPath + adjacencyList.get(nodeNum).get(j).getValue()) <
                        nodeList.get(curNodeNum).ShortestPath) {

                    synchronized (nodeList) {
                        nodeList.get(curNodeNum).ShortestPath = nodeList.get(nodeNum).ShortestPath +
                                adjacencyList.get(nodeNum).get(j).getValue();

                        nodeList.get(curNodeNum).PrevNode = nodeList.get(nodeNum).getName();
                    }
                }
            }

            releaseWait(busyNodes, curNodeNum);
        }
    }

    // Notifies all threads that current Node is no longer processes with this thread
    synchronized private void releaseWait(ArrayList<Integer> busyNodes,int curNodeNum) {
        Integer curNode = curNodeNum;
        busyNodes.remove(curNode);
        notifyAll();
    }

    // Sets thread to sleep if another thread is processing current Node in graph
    synchronized private void waitIfNeeded(ArrayList<Integer> busyNodes, int curNodeNum) {
        if (!busyNodes.contains(curNodeNum)) {
            busyNodes.add(curNodeNum);
        }
        else {
            while (!busyNodes.contains(curNodeNum)) {
                try {
                    System.out.println(Thread.currentThread().getName() + " thread is waiting");
                    Thread.sleep(_sleepTime);
                    System.out.println(Thread.currentThread().getName() + " thread stopped waiting");
                } catch (InterruptedException e) {
                    System.out.println("InterruptedException caught");
                }
            }
        }
    }
}