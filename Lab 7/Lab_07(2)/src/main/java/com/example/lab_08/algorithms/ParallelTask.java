package com.example.lab_08.algorithms;

import com.example.lab_08.utils.Pair;

import java.util.ArrayList;

public class ParallelTask implements Runnable {
    private ArrayList<Node> _nodeList;
    private ArrayList<ArrayList<Pair<Integer, Integer>>> _adjacencyList;
    private int _nodeNum;
    private ArrayList<Integer> _busyNodes;
    private final long _sleepTime = 1000;
    private String _name;
    private int _priority;
    private String _status;
    private boolean _isAlive;

    // This ctor should be only used as a mock object
    public ParallelTask() {
        setName("none");
        setPriority(-1);
        setStatus("none");
        setIsAlive(false);
    }

    public ParallelTask(ArrayList<Node> nodeList,
                        ArrayList<ArrayList<Pair<Integer, Integer>>> adjacencyList,
                        int nodeNum,
                        ArrayList<Integer> busyNodes) {

        _nodeList = nodeList;
        _adjacencyList = adjacencyList;
        _nodeNum = nodeNum;
        _busyNodes = busyNodes;
        setName("none");
        setPriority(-1);
        setStatus("none");
        setIsAlive(false);
    }

    // Updates thread status info
    // runs parallelTask() and marks node as visited
    // sleeps for _sleepTime to simulate heavy process
    @Override
    public void run() {
        setName(Thread.currentThread().getName());
        setPriority(Thread.currentThread().getPriority());
        setStatus(Thread.currentThread().getState().toString());
        setIsAlive(true);

        parallelTask(_nodeList, _adjacencyList, _nodeNum, _busyNodes);
        _nodeList.get(_nodeNum).isVisited = true;

        try {
            Thread.sleep(_sleepTime);
        } catch (InterruptedException e) {
            System.out.println("Exception in parallel task thread");
        }

        setIsAlive(false);
    }

    // Iterates over neighbor nodes and sets new Path if needed
    private void parallelTask(ArrayList<Node> nodeList,
                              ArrayList<ArrayList<Pair<Integer, Integer>>> adjacencyList,
                              int nodeNum,
                              ArrayList<Integer> busyNodes) {

        int curNodeNum = -1;

        for (int j = 0; j < adjacencyList.get(nodeNum).size(); j++) {

            curNodeNum = adjacencyList.get(nodeNum).get(j).getKey();
            waitIfNeeded(busyNodes, curNodeNum);

            if (!nodeList.get(curNodeNum).isVisited) {

                if((nodeList.get(nodeNum).ShortestPath + adjacencyList.get(nodeNum).get(j).getValue()) <
                        nodeList.get(curNodeNum).ShortestPath) {

                    synchronized (nodeList) {
                        nodeList.get(curNodeNum).ShortestPath = nodeList.get(nodeNum).ShortestPath +
                                adjacencyList.get(nodeNum).get(j).getValue();

                        nodeList.get(curNodeNum).PrevNode = nodeList.get(nodeNum).getName();
                    }
                }
            }

            releaseWait(busyNodes, curNodeNum);
        }
    }

    // Notifies all threads that current Node is no longer processes with this thread
    synchronized private void releaseWait(ArrayList<Integer> busyNodes,int curNodeNum) {
        Integer curNode = curNodeNum;
        busyNodes.remove(curNode);
        notifyAll();
    }

    // Sets thread to sleep if another thread is processing current Node in graph
    synchronized private void waitIfNeeded(ArrayList<Integer> busyNodes, int curNodeNum) {
        if (!busyNodes.contains(curNodeNum)) {
            busyNodes.add(curNodeNum);
        }
        else {
            while (!busyNodes.contains(curNodeNum)) {
                try {
                    System.out.println(Thread.currentThread().getName() + " thread is waiting");
                    Thread.sleep(_sleepTime);
                    System.out.println(Thread.currentThread().getName() + " thread stopped waiting");
                } catch (InterruptedException e) {
                    System.out.println("InterruptedException caught");
                }
            }
        }
    }

    // Getters and setters region
    public String getName() {
        return _name;
    }

    public void setName(String _name) {
        this._name = _name;
    }

    public int getPriority() {
        return _priority;
    }

    public void setPriority(int _priority) {
        this._priority = _priority;
    }

    public String getStatus() {
        return _status;
    }

    public void setStatus(String _status) {
        this._status = _status;
    }

    public boolean getIsAlive() {
        return _isAlive;
    }

    public void setIsAlive(boolean _isAlive) {
        this._isAlive = _isAlive;
    }
}
