package com.example.lab_08.algorithms;

// Class that represents Node in the graph
public class Node {
    private char _node;
    public double ShortestPath;
    public char PrevNode;
    public boolean isVisited;

    public Node(char nodeName, double initialPath)
    {
        _node = nodeName;
        ShortestPath = initialPath;
        PrevNode = '0';
        isVisited = false;
    }

    public char getName()
    {
        return _node;
    }
}