package com.example.lab_08.models;

import com.example.lab_08.algorithms.DijkstraAlgorithm;
import com.example.lab_08.algorithms.DijkstraSolution;
import com.example.lab_08.algorithms.ParallelTask;
import com.example.lab_08.utils.Pair;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;

import java.util.ArrayList;

public class AlgorithmModel {
    @FXML
    private final ObservableList<ThreadMonitorModel> monitors = FXCollections.observableArrayList();
    @FXML
    private final StringProperty solution = new SimpleStringProperty("");
    private ArrayList<ArrayList<Pair<Integer, Integer>>> _adjacencyList;
    private DijkstraAlgorithm _deijkAlgorithm;
    private DijkstraSolution _deijkSolution;
    private ArrayList<Thread> _monitorThreads;

    public AlgorithmModel(int threadCount) {
        // Set up
        _monitorThreads = new ArrayList<>();
        _adjacencyList = SetUpGraph();
        _deijkAlgorithm = new DijkstraAlgorithm(_adjacencyList, 'a', threadCount);

        // Connect Monitors to all threads in DijkstraAlgorithm and start them
        _monitorThreads = SetUpMonitorThreads(_deijkAlgorithm.getThreads());
        for (var t : _monitorThreads)
            t.start();

        // Define separate thread for Deijkstra's algorithm
        Thread algThread = new Thread() {
            @Override
            public void run() {
                setName("Algorithm thread");

                // Start DeijkstraAlgorithm and find solution
                long start = System.currentTimeMillis();
                _deijkSolution = _deijkAlgorithm.FindSolution();
                long finish = System.currentTimeMillis();
                setSolution(_deijkSolution.toString() +
                        "\nTime taken: " + (finish - start) + " millis");

                // Stop monitors
                for (var m : monitors)
                    m.stop();

                cleanUp();
            }
        };

        algThread.start();
    }

    // Create monitors, assign them threads to watch
    private ArrayList<Thread> SetUpMonitorThreads(ArrayList<ParallelTask> threadsToWatch) {
        ArrayList<Thread> mThreads = new ArrayList<>();

        for (int i = 0; i < threadsToWatch.size(); i++) {
            monitors.add(new ThreadMonitorModel(threadsToWatch, i));
            mThreads.add(new Thread(monitors.get(monitors.size() - 1), "monitor" + i));
        }

        return mThreads;
    }

    // Cleans up threads (perhaps)
    private void cleanUp() {
        for (int i = 0; i < _monitorThreads.size(); i++) {
            _monitorThreads.get(i).interrupt();
            _monitorThreads.set(i, null);
        }
    }

    // Creates graph as AdjacencyList
    private ArrayList<ArrayList<Pair<Integer, Integer>>> SetUpGraph() {
        ArrayList<ArrayList<Pair<Integer, Integer>>> adjacencyList = new ArrayList<>();
        ArrayList<Pair<Integer, Integer>> nodeTmp;
        // 0
        nodeTmp = new ArrayList<>();
        nodeTmp.add(new Pair<>(1, 4));
        nodeTmp.add(new Pair<>(7, 8));
        adjacencyList.add(nodeTmp);
        // 1
        nodeTmp = new ArrayList<>();
        nodeTmp.add(new Pair<>(0, 4));
        nodeTmp.add(new Pair<>(2, 8));
        nodeTmp.add(new Pair<>(7, 11));
        adjacencyList.add(nodeTmp);
        // 2
        nodeTmp = new ArrayList<>();
        nodeTmp.add(new Pair<>(1, 8));
        nodeTmp.add(new Pair<>(3, 7));
        nodeTmp.add(new Pair<>(5, 4));
        nodeTmp.add(new Pair<>(8, 2));
        adjacencyList.add(nodeTmp);
        // 3
        nodeTmp = new ArrayList<>();
        nodeTmp.add(new Pair<>(2, 7));
        nodeTmp.add(new Pair<>(4, 9));
        nodeTmp.add(new Pair<>(5, 14));
        adjacencyList.add(nodeTmp);
        // 4
        nodeTmp = new ArrayList<>();
        nodeTmp.add(new Pair<>(3, 9));
        nodeTmp.add(new Pair<>(5, 10));
        adjacencyList.add(nodeTmp);
        // 5
        nodeTmp = new ArrayList<>();
        nodeTmp.add(new Pair<>(2, 4));
        nodeTmp.add(new Pair<>(3, 14));
        nodeTmp.add(new Pair<>(4, 10));
        nodeTmp.add(new Pair<>(6, 2));
        adjacencyList.add(nodeTmp);
        // 6
        nodeTmp = new ArrayList<>();
        nodeTmp.add(new Pair<>(5, 2));
        nodeTmp.add(new Pair<>(7, 1));
        nodeTmp.add(new Pair<>(8, 6));
        adjacencyList.add(nodeTmp);
        // 7
        nodeTmp = new ArrayList<>();
        nodeTmp.add(new Pair<>(0, 8));
        nodeTmp.add(new Pair<>(1, 11));
        nodeTmp.add(new Pair<>(6, 1));
        nodeTmp.add(new Pair<>(8, 7));
        adjacencyList.add(nodeTmp);
        // 8
        nodeTmp = new ArrayList<>();
        nodeTmp.add(new Pair<>(2, 2));
        nodeTmp.add(new Pair<>(6, 6));
        nodeTmp.add(new Pair<>(7, 7));
        adjacencyList.add(nodeTmp);

        return  adjacencyList;
    }

    // Getters and setters region
    public ObservableList<ThreadMonitorModel> getMonitors() {
        return monitors;
    }

    public String getSolution() {
        return solution.get();
    }

    public StringProperty solutionProperty() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution.set(solution);
    }
}
