package com.example.lab_08.algorithms;

import com.example.lab_08.utils.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.*;

import static java.lang.Thread.MAX_PRIORITY;

public class DijkstraAlgorithm {
    private DijkstraSolution _solution;
    private ArrayList<Integer> _busyNodes;
    private ArrayList<Integer> _availableNodes;
    private int _threadCount;
    private ArrayList<ParallelTask> _threads;
    private ArrayList<ArrayList<Pair<Integer, Integer>>> _adjacencyList;

    public DijkstraAlgorithm(ArrayList<ArrayList<Pair<Integer, Integer>>> adjacencyList, char startingNode, int threadCount) {
        _adjacencyList = adjacencyList;
        _solution = new DijkstraSolution(_adjacencyList.size(), startingNode);
        _busyNodes = new ArrayList<>();
        _threadCount = threadCount;
        _threads = new ArrayList<>();
        _availableNodes = null;

        for (int i = 0; i < threadCount; i++)
            _threads.add(new ParallelTask());
    }

    // Finds SpanningTree of the graph using threads
    public DijkstraSolution FindSolution()
    {
        _availableNodes = getAvailableNodes(_solution.NodeList);
        ExecutorService threadPool = Executors.newFixedThreadPool(_threadCount);

        // iterate until there is no unvisited vertex
        while(_availableNodes.size() != 0) {
            Collection<Future<?>> tasks = new ArrayList<>();

            // call ParallelTask for all available nodes on cur iteration
            for (int i = 0; i < _availableNodes.size(); ) {
                // spread tasks across all threads
                for (int j = 0; j < Math.min(_availableNodes.size(), _threadCount) && (_availableNodes.size() - i) != 0; j++) {

                    _threads.set(j, new ParallelTask(
                            _solution.NodeList,
                            _adjacencyList,
                            _availableNodes.get(i),
                            _busyNodes));

                    tasks.add(threadPool.submit(_threads.get(j))); //_threads.get(j)
                    i++;
                }

                for (Future<?> currTask : tasks) {
                    try {
                        currTask.get();
                    } catch (Throwable thrown) {
                        System.out.println("Error while waiting for thread completion");
                    }
                }
            }

            _availableNodes = getAvailableNodes(_solution.NodeList);
        }

        cleanUp(threadPool);
        return _solution;
    }

    // Returns threads used by algorithm
    public ArrayList<ParallelTask> getThreads() {
        return _threads;
    }

    // Returns available nodes to process
    private static ArrayList<Integer> getAvailableNodes(ArrayList<Node> nodeList) {
        ArrayList<Integer> availNodes = new ArrayList<>();

        for (int i = 0; i < nodeList.size(); i++) {
            if (!nodeList.get(i).isVisited && Double.isFinite(nodeList.get(i).ShortestPath)) {
                availNodes.add(i);
            }
        }

        return availNodes;
    }

    // Cleans up threads (perhaps)
    private void cleanUp(ExecutorService threadPool) {
        threadPool.shutdown();
        try {
            threadPool.awaitTermination(MAX_PRIORITY, TimeUnit.HOURS);
        } catch (InterruptedException e) {
            System.out.println("Exception while waiting for thread pool to close");
        }
        //for (int i = 0; i < _threads.size(); i++) {
        //    _threads.get(i).interrupt();
        //    _threads.set(i, null);
        //}
    }
}

