package com.example.lab_08.utils;

// Custom Pair class realisation as Jaba doesn't have one
public class Pair<K, V> {
    private K _key;
    private V _value;

    public Pair(K key, V value) {
        _key = key;
        _value = value;
    }

    public K getKey() {
        return _key;
    }

    public V getValue() {
        return _value;
    }

    @Override
    public String toString() {
        return "Key: " + _key + ", Value: " + _value;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof Pair)) {
            return false;
        }

        Pair p2 = (Pair) o;

        return _key.equals(p2._key) && _value.equals(p2._value);
    }
}
